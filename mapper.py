import mongoengine

""" Use DynamicDocument instead of Document
for ingoring schema validation.
"""
from mongoengine import DynamicDocument


class IntentLabel(DynamicDocument):
    meta = {'collection': 'intents'}
    label = mongoengine.StringField()
    _id = mongoengine.ObjectIdField()
    engineId = mongoengine.ObjectIdField()


class IntentSample(DynamicDocument):
    meta = {'collection': 'sample-inputs'}
    engineId = mongoengine.ObjectIdField()


class Keywords(DynamicDocument):
    meta = {'collection': 'keywords'}
    engineId = mongoengine.ObjectIdField()


class Entity(DynamicDocument):
    meta = {'collection': 'entity-types'}
    _id = mongoengine.ObjectIdField()
    isSystem = mongoengine.BooleanField()
    engineId = mongoengine.ObjectIdField()
