""" This module be used to get data and convert it
to comfortable format to train with NLP Engine.

------
Author: b.giahuy (hosjiu)
Email: hosjiu1702@gmail.com
"""

from typing import List, Dict, Union
import os
from os.path import dirname
from pathlib import Path

import mongoengine
from mongoengine import Document
from bson.objectid import ObjectId
from tqdm import tqdm
import click
from dotenv import load_dotenv

from mapper import IntentLabel, IntentSample, Keywords, Entity


# env_path = Path('..') / '.env'
env_path = '/home/emandai/workspace/vnlp/src/vnlp.engine.ge/.env'
load_dotenv(dotenv_path=env_path)
# GENERAL_ENGINE_ID = os.getenv("GENERAL_ENGINE_ID")
GENERAL_ENGINE_ID = "5dd55bb3007458fe66d0c0c6"

DEBUG = True
HOST = "mongodb+srv://admin:8FSUGhsEj20JCJZ3@dev-fdqci.gcp.mongodb.net/nlp?retryWrites=true&w=majority"

"""
- Temporary intent documents for using later to improve performance.
- Using global variable is not a good idea. So if you have another
idea, I always appreciate about that.
"""
INTENT_DOCS = []
SAMPLE_DOCS = []


def connect_db(host: str) -> None:
    mongoengine.connect(host=host)


def get_intents(engine_id: str) -> List[str]:
    # Need improve?
    intents = []
    r_intents = IntentLabel.objects(engineId=engine_id)
    for r_intent in tqdm(r_intents):
        if r_intent['isActived'] == 1:
            intents.append(r_intent['field'])
            global INTENT_DOCS
            INTENT_DOCS.append(r_intent)

    return intents


# def get_entites_types(engine_id: str) -> List:


def _get_intent_by_id(_id: ObjectId) -> Union[Document, None]:
    """ This func is *locally* used to choose proper intent document
    from *_id* field based on *INTENT_DOCS* global variable.
    """
    global INTENT_DOCS
    INTENT_DOCS_ = INTENT_DOCS

    assert len(INTENT_DOCS_) != 0, "Must run get_intents() before do this func."
    
    for doc in INTENT_DOCS_:
        if doc['_id'] == _id:
            return doc

    return None


def get_samples(engine_id: str) -> List[List]:
    samples = []
    r_samples = IntentSample.objects(engineId=engine_id)
    for r_sample in tqdm(r_samples):
        if r_sample['isActived'] == 1:
            global SAMPLE_DOCS

            user_message = r_sample['content']
            foreign_key = r_sample['intent']
            samples.append([user_message, foreign_key])
            SAMPLE_DOCS.append(r_sample)

    return samples


def get_intent_training_data(engine_id: str) -> List[List]:
    intents = get_intents(engine_id)
    training_data = []
    samples = get_samples(engine_id)
    
    # Pull intent back from foreign key (_id of intents).
    for sample in tqdm(samples):
        r_intent = _get_intent_by_id(sample[1])
        diagnosis_msg = "\n- Please re-check your get_intents() func.\n" +\
                    "- *isActived* value should be check with proper value is 1."
        assert r_intent is not None, diagnosis_msg
        training_data.append([r_intent['field'], sample[0]])
    
    return training_data


def get_keywords_training_data(engine_id: str) -> List[List]:
    keywords = []
    keyword_docs = Keywords.objects(engineId=engine_id)
    for keyword in tqdm(keyword_docs):
        if keyword['isActived'] == 1:
            synonyms = []
            for synonym in keyword['synonym']:
                synonyms.append(tuple((keyword['key'], synonym)))
            keywords.append(synonyms)

    return keywords


def get_ner_training_data(engine_id: str) -> List[List]:
    ner_data = []
    keyword_docs = Keywords.objects(engineId=engine_id)
    for keyword_doc in tqdm(keyword_docs):
        if keyword_doc['isActived'] == 1:
            entity_type = Entity.objects(_id=keyword_doc['entityType']).first()
            ner_data.append([entity_type['type'], keyword_doc['key']])

    return ner_data


def get_another_ner_training_data(engine_id: str) -> List[List]:
    another_ner_data = []
    global SAMPLE_DOCS
    SAMPLE_DOCS_ = SAMPLE_DOCS

    assert len(SAMPLE_DOCS_) != 0, "Empty samples. Must run get_samples() before do this func."

    if engine_id == GENERAL_ENGINE_ID:
        entity_type_docs = Entity.objects(__raw__={'isSystem': True})
    else:
        entity_type_docs = Entity.objects(engineId=engine_id)

    def _get_entity_type_doc_by_id(_id):
        for e_doc in entity_type_docs:
            if _id == e_doc._id:
                return e_doc
        return None

    for idx, s in enumerate(SAMPLE_DOCS_):
        entities = s['entities']
        another_ner_data.append([s['content'], []])
        ui_flag = True
        for e in entities:
            try:
                entity_doc = _get_entity_type_doc_by_id(_id=e['entityType'])
                entity_type = entity_doc.type
                pair = tuple((e['entity'], entity_type, e['begin']))
                another_ner_data[idx][1].append(pair)
            except KeyError:
                another_ner_data.pop(-1)
                ui_flag = False
            except AssertionError:
                msg = f"please check the entity-types collection in your MongoDB at {HOST}. (EMTPY COLLECTION)"
                print(msg)
            except IndexError:
                continue
        try:
            if ui_flag:
                if len(another_ner_data[idx][1]) != 0:
                    another_ner_data[idx][1] = sorted(another_ner_data[idx][1], key=lambda x: x[2])
        except IndexError:
            continue

    return another_ner_data


def generate_keyword_file(keyword_data: str, data_path: str) -> None:
    out_path = os.path.join(data_path, 'keywords.txt')
    with open(out_path, 'w') as f:
        for k in keyword_data:
            if len(k) == 0: 
                continue
            f.write(f"{k[0][0]};")
            end_of_index = len(k) - 1
            for idx, synonym in enumerate(k):
                f.write(synonym[1])
                if idx != end_of_index:
                    f.write(',')
            f.write("\n")
    f.close()


def generate_intent_file(intent_data: str, data_path: str) -> None:
    out_path = os.path.join(data_path, 'intents.txt')
    with open(os.path.join(out_path), 'w') as f:
        for i in intent_data:
            f.write(f"__label__{i[0]} , {i[1]}\n")
    f.close()


def generate_ner_file(ner_data: str, data_path: str) -> None:
    out_path = os.path.join(data_path, 'ner.txt')
    with open(out_path, 'w') as f:
        for n in ner_data:
            f.write(f"{n[0]};{n[1]};{n[1]}\n")
    f.close()


def generate_another_ner_file(ner_data: str, data_path: str) -> None:
    out_path = os.path.join(data_path, 'another_ner.txt')
    with open(out_path, 'w') as f:
        for data in ner_data:
            if len(data[1]) > 0:
                f.write(f"{data[0]} ")
                end_of_index = len(data[1]) - 1
                for idx, p in enumerate(data[1]):
                    f.write(f"{p[0]}|{p[1]}")
                    if idx != end_of_index:
                        f.write(';')
                f.write('\n')
    f.close()


def generate_files(engine_id: str) -> None:
    assert type(engine_id) is str
    # Get data for preparing to create files
    keyword_data = get_keywords_training_data(engine_id=engine_id)
    ner_data = get_ner_training_data(engine_id=engine_id)
    intent_data = get_intent_training_data(engine_id=engine_id)
    another_ner_data = get_another_ner_training_data(engine_id=engine_id)

    # Gen files
    DATA_PATH = os.path.join(dirname(os.path.abspath(__file__)), 'data')
    if not os.path.isdir(DATA_PATH):
        os.mkdir(DATA_PATH)
    generate_keyword_file(keyword_data, DATA_PATH)
    generate_ner_file(ner_data, DATA_PATH)
    generate_intent_file(intent_data, DATA_PATH)
    generate_another_ner_file(another_ner_data, DATA_PATH)


def debug() -> None:
    connect_db(host=HOST)

    engine_id = "5ffd7aac26a14f760f4c0002"

    """ Intent Data """
    t = get_intent_training_data(engine_id=engine_id)
    print(f"INTENT: {t}\n")

    """ Keywords Data """
    k = get_keywords_training_data(engine_id=engine_id)
    print(f"KEYWORD: {k}\n")

    """ NER Data """
    ner = get_ner_training_data(engine_id=engine_id)
    print(f"NER: {ner}")

    # Gen files
    generate_files(engine_id=engine_id)

    # Status check
    print("\nOK.")


@click.command()
@click.argument('engine_id')
def run(engine_id: str) -> None:
    connect_db(host=HOST)
    generate_files(engine_id)


if __name__ == '__main__':

    if DEBUG:
        debug()
    else:
        run()
