import mongoengine

""" Use DynamicDocument instead of Document
for ingoring schema validation.
"""
from mongoengine import DynamicDocument
# from mongoengine.queryset import QuerySet

HOST = "mongodb+srv://admin:8FSUGhsEj20JCJZ3@dev-fdqci.gcp.mongodb.net/nlp?retryWrites=true&w=majority"

# class RawData(QuerySet):
#     def __call__(self):
#         pass

class IntentLabel(DynamicDocument):
    meta = {'collection': 'intents'}
    field = mongoengine.StringField()
    _id = mongoengine.ObjectIdField()
    engineId = mongoengine.ObjectIdField()


class IntentSample(DynamicDocument):
    meta = {'collection': 'sample-inputs'}
    sample = mongoengine.StringField()
    engineId = mongoengine.ObjectIdField()


class Keywords(DynamicDocument):
    meta = {'collection': 'keywords'}
    synonym = mongoengine.ListField()
    engineId = mongoengine.ObjectIdField()


if __name__ == '__main__':
    # rawdata = RawData(document='', collection='sample-inputs')
    # print(rawdata.all()
   
    """ Connect to MongoDB Server """
    mongoengine.connect(host=HOST)

    # Get all document in the collection
    # raw_intents = IntentLabel.objects(engineId="5ffd7aac26a14f760f4c0002").all()
    # print(dict(raw_intents[0].to_mongo())['_id'])
    # print('--------\n')

    # # Print out the field *Field* for each document
    # for raw_intent in raw_intents:
    #     # convert document to dict
    #     raw_intent = dict(raw_intent.to_mongo())
    #     print(raw_intent['field'])

    # Get intent from _id attribute
    # label = IntentLabel.objects(_id="5ffd7aad9c17c4588d5fc8e2").all()
    # # label = IntentLabel.objects(field='intent_a')
    # print(label)

    docs = Keywords.objects(engineId="5ffd7aac26a14f760f4c0002").all()
    print(docs)
    for doc in docs:
        import pdb;pdb.set_trace()
        for idx, synonyms in enumerate(doc):
            print(f"KEYWORD {idx+1}: \t")
            for synonym in synonyms:
                print(synonym)





